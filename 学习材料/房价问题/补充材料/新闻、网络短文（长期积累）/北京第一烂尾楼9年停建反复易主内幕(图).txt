北京第一烂尾楼9年停建反复易主内幕(图) 

-------------------------------------------------------------------------------- 

http://www.sina.com.cn 2006年03月31日05:16 新京报 



位处CBD繁华区的中宇大厦经历9年烂尾之后，有望于今年复工。本报记者韩萌摄 

点击此处查看全部新闻图片 



　　□本报记者 王全宝 北京报道 

　　-核心提示 

　　“京城第一烂尾楼”中宇大厦今年有望复工。 








　　在9年烂尾历程中，部分产权数易其主，资金链经常断裂。 

　　2005年，北京市建委通过对尚存的16个烂尾楼调查，发现“烂尾”的主要原因系开发商资金实力不足。 

　　就中宇大厦来讲，贱卖、空手套楼伴随其股权转让始终。历经眼花缭乱的拍卖与裁定程序后，北京安石房地产公司成为一大赢家，新近入股的北京城乡也有望分得一杯羹。 

　　而昔日烂尾楼能否重启建设仍有困难。 

　　3月21日上午，三里屯酒吧街与工体北路交汇处，一处建筑工地上，26层高的大楼裸露着水泥。 

　　一群身着迷彩服、头戴蓝色安全帽的建筑工人与工地保安发生冲突，砖头、瓦块、石头横飞，门外建筑工人推开铁门，拥进工地。 

　　5分钟后，事态得到警方控制。警方事后证实，冲突双方为新旧业主分别雇请的施工单位。 

　　这座灰色的大楼就是号称“京城第一烂尾楼”的中宇大厦，始建于1997年。 

　　过去9年间，这座106米高的大厦数易其主，纠纷不断，伴随其中的是逾两亿资金的流动。 

　　今年2月份，上市公司北京城乡(600861)购得部分产权，计划重启工程建设，但潜于其后的产权纠纷仍未理清。 

　　这一年，被列为北京市烂尾楼集中处理年。 

　　据北京市建委消息，截至2005年11月底，北京市仅剩16个烂尾楼，“烂尾”的主要原因是开发商资金实力不足，出现资金链断裂。 

　　据了解，北京市建委、规划委等4部门已于近期联手展开新一轮调研工作。在上述16个烂尾楼项目中，目前正筹备复工的项目有6个，中宇大厦位列其中。 

　　一个“怪胎”的出生 

　　两股东实际上均为中华咨询的子公司，3公司签有“君子协定”，北京银企的1000万注资由中华培训提供，董事长由中华培训公司法人刘位申担任。 

　　北京CBD繁华地带，中宇大厦与港澳中心等众多高档写字楼比邻而居，顶部帆形标志煞是显眼，寓意扬帆航行，但这艘“大船”从一诞生就不顺利。 

　　1997年年初，中宇大厦的起建起源于两家国有公司的联合开发。 

　　当年，财政部下属企业中华高级财会人员培训中心(简称中华培训，后更名为华信培训发展中心)因业务发展需要建立自己的办公场所，便和北京机械工业管理局(后变更为北京京城机电控股公司)在该局所属工体旁的一所小学原址上联建一座大厦。 

　　1997年1月20日，针对此项目建设的北京银企房地产开发有限公司(简称“北京银企”)“应运而生”，注册资本1000万元。 

　　从工商资料来看，银企公司和中华培训并无关系，两股东分别为中华财务会计咨询公司和华正财会软件开发公司，两家公司分别投资750万元和250万元。 

　　而据央视《焦点访谈》报道，两股东实际上均为中华咨询的子公司，3公司签有“君子协定”，北京银企的1000万注资由中华培训提供，董事长由中华培训公司法人刘位申担任。 

　　为何做出上述的安排？ 

　　对此，刘位申此前在接受媒体采访时介绍，根据《公司法》等相关法律条文规定，注册公司必须有两个法人股东，此种情况下，中华培训“相中”两子公司。 

　　“不参加利益分配，也不承担任何投资风险”。按照刘位申的介绍，两子公司考虑风险不愿意出资，仅作为名义股东注册公司。 

　　之后，北京银企和北京机械工业管理局联建银企大厦，后改名中宇大厦，双方协议，机械局负责提供大厦的土地使用权，银企公司负责大厦的建设资金，建成以后，银企公司分得36040平方米的建筑面积，机械局分得16100平方米的建筑面积，施工方为中国建筑第一工程局第五建筑公司(简称中建五公司)。 

　　另据央视此前报道，银企成立之后，中华培训从海南省一家国有投资公司以及其他公司借来8000万元，借款期限是1年。 

　　对于上述情况，中华咨询的主管单位财政部当时并不知情。 

　　首度转手 

　　金峰阁公司此后不仅未有进一步购买中宇大厦产权的行动，其对银企公司的受让款也未如期交出。 

　　1999年年底，中宇大厦建到13层时突然停工。 

　　来自原银企公司内部员工的消息是，银企的资金链断了。 

　　一个背景是，在1999年年底，作为大股东的中华咨询公司向财政部提请脱钩改制，并在次年6月份，由国企改为民营企业。 

　　在此期间，银企公司也开始寻找新的投资方。 

　　1999年12月26日，北京银企与北京金峰阁房地产开发有限公司(以下简称“金峰阁”)，签订《转让合同》。 

　　金峰阁公司是一家民营企业，成立于1999年8月31日，注册资本3000万元。 

　　按照合同约定，银企公司将其所有的权益转让给金峰阁，转让价款为8100万元，另外，北京银企还将获得大厦第25层的产权。 

　　在合同中，表述的金峰阁的最终目的是完全取得中宇大厦的全部权益。银企公司董事长刘位申接受央视采访时称，在此合同背后还有一项“隐合同”，即金峰阁欲用2600万元购买机械局所属的产权。 

　　在此前提下，双方约定，金峰阁要在2000年10月30日前将款项付给银企公司。 

　　但事实上，金峰阁公司此后不仅未有进一步购买中宇大厦产权的行动，其对银企公司的受让款也未如期交出。 

　　另一方面，在随后中宇大厦的复工建设中，金峰阁的投入也非自有资金。 

　　在接手部分产权后，金峰阁立即展开的融资方式是，以个人按揭形式向外发售。后来，共59户进行了按揭。 

　　此举给金峰阁公司带来的好处是，工商银行望京支行贷给中宇大厦1.1亿元，同时作为施工企业的中建五公司也垫付了4000多万元的启动资金。至此，中宇大厦项目造价已达到2.5亿元。 



再次停建 

　　《关于金峰阁公司中宇大厦项目发放拯救性贷款1.5亿元的审查报告》显示，银行称金峰阁公司出资没有到位，此前的贷款也有问题。 

　　事实上，当时，中宇大厦并不能向外发售房屋。 








　　今年3月份，金峰阁公司有关负责人向本报记者介绍，当时，中宇大厦并未办理土地使用性质改变手续。 

　　该负责人介绍，中宇大厦原属基建项目，所建大厦只能自用，如果向外发售，需要转为商品开发项目。 

　　但这家公司，最后也没能使这座大厦起死回生。 

　　2001年春节后，中宇大厦在主体结构即将封顶时再次停建。 

　　“因为金峰阁没有钱了。”李国英，原金峰阁总经理兼财务总监，2000年12月28日，她接任董事长职位，今年3月20日，她介绍，直到2002年年初，金峰阁向北京神圣投资有限公司(以下简称“神圣公司”)借款2000万元，用部分款项到国土部门办理了大厦的土地证，才使大厦转为开发项目，使银行的贷款变为合法化。 

　　这一迟到的合法身份，并未改变中宇大厦的烂尾状况。 

　　李国英向本报记者介绍的情况是，2002年10月份，工商银行望京支行同意再接着给金峰阁公司贷款，但条件是金峰阁公司用整栋大厦做抵押，这需要金峰阁把已发售的房屋回购。 

　　2006年3月23日，知情人提供的2002年工商银行内部资料《关于金峰阁公司中宇大厦项目发放拯救性贷款1.5亿元的审查报告》显示，银行称金峰阁公司出资没有到位，此前的贷款也有问题。 

　　这种情况下，金峰阁的二次融资破灭。紧接着，金峰阁换了老板。 

　　2003年3月3日，李国英将金峰阁公司51%的股权转让给神圣公司，转让金共计6000万元。 

　　实际上，神圣公司支付给李国英500万元，并变更了企业法人代表，之后，也未再支付股权转让款给金峰阁。 

　　烂尾楼再易主 

　　2003年9月，未获得转让款的银企公司向北京仲裁委员会对金峰阁公司提起仲裁。10月，法院将金峰阁公司所属部分房产予以查封。 

　　神圣公司控股金峰阁之后，作为中宇大厦的业主，并没有复工再建。 

　　半年之后，已经和中宇大厦脱离产权关系的银企公司，其股东发生变动，其后的事实表明，这一变动和后来中宇大厦的最终走向存有关联。 

　　2003年9月15日，中华咨询公司将其持有的银企公司全部股份(占银企公司总股本的75%)转让给自然人于伟，价格是750万元，于伟的身份是北京市恒基伟业电子产品有限公司总裁助理。 

　　9月19日，缔造银企诞生的刘位申被免去董事长职位，10月26日，于伟接任。 

　　“作为名义股东，两家公司是无权处置银企公司的股权的。”刘位申当时接受媒体采访时坚称。 

　　于此之际，2003年9月，未获得转让款的银企公司向北京仲裁委员会对金峰阁公司提起仲裁。 

　　2003年10月9日，北京市第二中级人民法院将金峰阁公司在该大厦享有房产份额的3万平方米予以查封。 

　　2003年12月16日，北京仲裁委员会做出调解书，确认银企公司对金峰阁公司的债权数额由四年前的8100万元升为1.2亿元，并约定如逾期还款则以金峰阁公司名下中宇大厦房屋(实为在建工程)抵债，包括工商银行已经通过诉讼主张债权办理了预售登记的8400平方米房屋。 

　　按照此前金峰阁、神圣等公司负责人介绍，银企公司成立后，并没有经营其他项目，这样除了此前借来的8000万元借款和对金峰阁1.2亿元的债权外，再无大项负债和资产。 

　　如果上述情况成立，可以推算的是，于伟投资750万元获得银企公司75%的股份，实际上已经拥有1.2亿元债权和8000万元负债相抵后权益的75%.但是，对于当时银企公司的资产负债情况，记者无法查证，同样没有证据显示，于伟和中华咨询之间有何关联。 

　　1.2亿元的债权反映在中宇大厦上，可以计算的是，通过当时工商银行望京支行和中建五公司分别对金峰阁提请1.3亿元和7600万元的债权主张可以看出，不计此前银企自身投入，当时大厦建设投入的成本已经超过两亿元。 

　　一个事实是，于伟正式掌控银企公司后，将他在银企公司全部权利转让给北京安石投资管理有限公司(以下简称安石投资公司)，诸如转让款等情况不得而知。 

　　“完璧归赵” 

　　法院裁定安石投资公司为中宇大厦买受人，价格为1.4亿元，并将金峰阁名下约3.56万平方米在建工程过户到安石名下。安石公司也是金峰阁1.2亿元欠款的债权人。 

　　之后，银企公司注销登记，中宇大厦也进入强行拍卖程序。 

　　2004年3月25日，安石投资公司向北京市高级人民法院申请强制执行仲裁调解书。6月23日，北京市高院执行庭主持对金峰阁公司名下中宇大厦进行拍卖。 

　　相关资料显示，当时北京高院委托有关机构对中宇的评估价格为1.56亿元，在金峰阁公司2004年6月17日送交最高人民法院执行办公室的“紧急情况”反映材料中，其出具的2001年经北京中威君平房地产评估有限公司对中宇的评估价值为5.4亿元。 

　　最后，中都国际拍卖有限公司所定的起拍价为1.2亿元，北京圣科投资管理有限公司(以下简称圣科公司)以1.4亿元成为买受人。 

　　安石投资公司作为该案申请执行人也参与了竞拍，但未成交。圣科公司竞拍成功后，并未全额支付竞买价款。 

　　2004年11月和2005年1月14日，北京高院分别以(2004)高执字第66号和(2004)高执字第66-1号民事裁定书两次裁定安石投资公司为中宇大厦买受人，价格为1.4亿元，并将金峰阁名下约3.56万平方米在建工程过户到安石名下。 

　　2005年1月11日，由北京安石投资管理有限公司和北京恒基广业技贸有限责任公司注资1000万元成立北京安石房地产开发公司(简称安石房地产公司)，准备再建中宇。 

　　对于上述的两份裁定，直到今年1月10日，安石房地产派人到现场施工时，金峰阁原董事长李国英和工商银行望京支行、中建五公司等才从安石处看到。 

　　但上述的两份裁定书和1月13日北京市高院提供给李国英等人的裁定书版本却不相同。肉眼可见，除内容相同外，排版格式和刻章位置均不相同，且其中一份在日期中只打印了年和月。 

　　“卖得太便宜了。”李国英认为，拍卖和裁定程序也有问题。 

　　北京市中高盛律师事务所主任胡凤斌介绍，两次流拍才能启动裁定程序，就中宇大厦而言，第一次买受人没有付款，应该启动第二次公开拍卖程序，而并非是直接裁定。 

　　“就算裁定前还应该咨询一下我们的意见。”现任金峰阁总经理的李国英还认为，作为业主的金峰阁和其他买家合作，有能力偿还债务，不应该裁定给安石公司，为此，李国英多次找到北京市高级法院。 

　　3月21日，北京高院门口传达室，执行庭副庭长陈海欧给李国英的答复是，他将把李国英反映的情况向单位汇报。 

　　对于上述的情况，北京市仲裁委员会和北京市高院婉拒了本报记者的采访。 

　　谁是最大赢家？ 

　　安石公司以1.4亿元获得原金峰阁大厦的3万余平方米，而转让了不到其中一半即获得了1.2亿元的回报。 

　　2006年2月16日，北京城乡发布公告称，北京安石房地产开发有限公司将北京中宇大厦1.5万平方米写字楼的产权转让给北京城乡，单价为每平方米平均受让价8000元，共计1.2亿元。 

　　这意味着，中宇大厦再次历经转让后，有望重启部分工程的封顶和装修。 

　　2006年3月22日，大厦施工现场，重新接手中宇大厦建设的青岛建设集团和原施工单位发生冲突，出现本文开头一幕。 

　　在此背后，北京安石成为最大的赢家，其以1.4亿元获得原金峰阁大厦的3万余平方米，而转让了1.5万平方米即获得了1.2亿元的回报，而安石原先对金峰阁提请的债权刚好为为1.2亿元。 

　　对于北京城乡来讲，其期待的收益在于贱买和后期经营收益。 

　　这一点在北京城乡的公告中可以明显看出，其介绍，经专业评估，该1.5万平方米写字楼房地产评估净值为1.9亿余元，每平方米单价为1.27万元。 

　　另据2月21日《北京日报》报道，在工体北路黄金圈，与中宇大厦相邻的4号院地块平均楼面价每平方米11000元。 

　　对于经营收益，北京城乡委托北京安石经营该部分写字楼，双方已约定经营年收益率不低于10%，若当年收益率低于10%，北京安石房地产公司负责补齐不足部分；如收益率高于10%，则超出部分双方同比例分成。该项目整体经营的年度净收益中，优先保证北京城乡年度净收益不低于1200万元。 

　　但在北京市中高盛律师事务所主任胡凤斌看来，北京城乡此次股权转让存在巨大风险，因为存在诉讼纠纷，最后很有可能产权难以过户。 

　　3月22日，本报记者拨通北京城乡董秘赵磊的电话，他表示不能接受采访，建议记者看一下上述的公告。 

　　另一个消息是，据相关知情人介绍，原北京银企公司的刘位申已在安徽被警方调查，目前情况如何不得而知。 

　　但上述消息并未获得求证。2006年3月23日，本报记者致电刘位申的妻子，刘妻表示无法联系到刘位申本人。 

