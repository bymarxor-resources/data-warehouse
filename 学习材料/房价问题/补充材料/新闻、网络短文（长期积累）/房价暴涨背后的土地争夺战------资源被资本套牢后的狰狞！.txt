房价暴涨背后的土地争夺战------资源被资本套牢后的狰狞！  
[ 吸呼 ] 于2007-07-27 00:56:35 上帖 [ 发短信 ] [ 表状 ]  
 


　　新一轮全国性房价上涨已经开始，土地也随之成了抢手货。令地产商为难的是，拿不到地，很可能会被淘汰出局，但多拿了地，又意味着要承担诸多风险。 

　　房价将持续上涨 

　　“新一轮全国性房价上涨已经开始。”7月13日，瑞士信贷第一波士顿首席经济学家陶冬在博客中判断。 

　　7月23日，国家发改委公布的调查报告显示，今年6月，全国70个大中城市房屋销售价格同比全部上涨，涨幅7.1%，创下23个月来新高。 

　　陶冬的预测基于三个基本发现：银行储蓄将向楼市转移；土地的供给不足导致未来可售面积下降，造成房源紧张；在当前的市场情况下，开发商的现金流得到明显改善，它们有了捂盘不卖的本钱。 

　　事实上，在新一轮上涨中，地价与房价交错上攻的趋势越来越明显。 

　　2001 年，我国开始实行土地收购储备制度，在这一制度下，地方政府通过土地收购储备机构成为了土地一级市场的垄断供应者。在土地收购储备制度之外还辅之以招拍挂，招拍挂下“价高者得”，以保证地方政府成为土地出让收益的最大受益者。但客观上“价高者得”的成交规则，在土地供给紧张的局势下，直接推动了地价上涨。拍卖地价不断创出新高，以致很多新拍出的地价比周边正在卖的楼盘房价还高。 

　　6月21日下午，浙江绿城让上海楼市吃了一惊。仅仅五六分钟的时间，新江湾城D1地块便以12.6亿元的“天价”，落入浙江绿城手中。掐指一算，楼面地价就在12509元／平方米。而同一天，新江湾城的楼盘售价，刚刚超过万元。 

　　“上海市区内，好的土地越来越少。”一名开发商坦言，“近几次土地出让，外环以内的土地都很少。像新江湾城这样的地块，配套设施完善，增值潜力巨大，当然受到追捧。” 

　　话虽如此，但12509元／平方米的楼面地价，依旧让人感到疑惑。新江湾城公开出让的前两块地，前年及去年由合生及华润争得，面积分别达18万平方米和14万平方米，楼面地价分别只有5617元 ／ 平方米和6676元／平方米。换句话说，同处新江湾城，楼面地价半年涨了一倍。 

　　业界普遍认为，以绿城拿地的价格，未来楼盘的房价将肯定在2万元以上。因为，土地价格往往占房价的50%-60%，“否则，开发商很难有合理的利润”。这一推断，获得了绿城掌门人宋卫平的认同。 

　　一个不容忽视的细节是，新江湾城位处上海人传统观念的“大杨浦”地区，离开上海的中心区域人民广场，需要横跨三个区，至少有一小时车程。“那里的房子要卖2万，那市中心的房子呢？”一些“老上海”不禁发问。 

　　浙江绿城的“标本价格”，还给其他开发商一些暗示。一名松江的开发商在得知宋卫平的出价后，立刻表示会“重新考虑”自己楼盘的价格定位，“简单比较谁都会，几年后是花2万元买杨浦区的房子，还是花9000元买松江的房子？”而松江则位于上海的郊区，更加边远。 

　　新江湾城D1地块的拍卖，再次引发了人们关于“地价VS房价”的争论。高地价增加了商品房成本，客观上抬高了房价。而房价上涨，又增加了开发商以高价拿地的能力。 

　　土地卖方市场 

　　“上海今年的土地供应量原定1300公顷，但到目前为止实际的供应量连300公顷都不到。”上海五合智库投资顾问公司总经理邹毅说，“很多地块没有达到开发条件，城市中心的拆迁难更加制约了一级市场的土地供应，因此现在是典型的土地卖方市场。” 

　　2006年，各地土地的招拍挂价格一路走高，开发商几乎不惜成本地大肆拿地。2007年，这种状况有过之而无不及。 

　　在招拍挂的一级市场，主要是万科、绿城这样的一线品牌竞争之地，但是对于很多二线地产品牌来说，资金实力的限制并不支撑他们参与这样“高昂的游戏”，于是他们将目光转向土地二级市场。 

　　这个市场的供应方主要是前期囤积了一些土地的开发商，但是由于资金链或者其他原因他们想转让手中的土地。 

　　目前，京城二手土地中介的生意也做得热火朝天，因为这些中介手中的“货”，对一些开发商来说更有吸引力，赵小冬或许就是二手土地中介中生意做得不错的一个。 

　　记者从赵小冬提供的资料中看到，他的手里一共有8个能够马上转让的土地项目公司，其中望京的项目标明“建筑面积18万平方米，项目所在地已完成‘七通一平’，原开发商提出项目整体转让”。 

　　目前，北京注册的房地产公司多达4000家，而正在进行地产开发的房地产公司仅为1000家左右，其余则为主要以出让股权的方式进行土地买卖的土地公司。 

　　而赵小冬这类土地中介公司的主要任务就是帮助这些土地公司将其手中的土地转让出去，通过他们的牵线搭桥来实现这种交易的快速进行。据了解，这类中介公司为数不少，他们的佣金大概是成交土地总金额的千分之三，有的还更高，主要是依据中介和交易双方谈判的结果而定。 

　　虽然二级市场上的地价比一级市场相对便宜，但也蕴含了很多风险。“真正拿过来就能开发的只是少数，这些地块的权属关系往往比较复杂，隐含了很多债务风险，在前期转让时并不容易发现。”北京一位知名房地产开发公司投资总监谈道，“介绍过来的项目很多，但真正符合开发要求的并不多，碰到一块好的地，争抢的激烈程度并不亚于一级市场。” 

　　“房地产企业都在不断扩大土地储备，表面上是为了应对未来增长的需要，实际上是对土地稀缺和地价上升的恐惧。”阳光100集团董事长易小迪说。 

　　高价拿地的风险 

　　目前的这种状况使得开发商就像穿上了红舞鞋，如果不去想办法拿地，他就将被淘汰出局，而如果去拿地，就必须忍受地价上涨带来的风险。 

　　幸运的是，在一个资本追逐资产的年代，谁掌握了土地就掌握了产业链的上游，就会得到资金的青睐。 

　　“我们更看重的是开发商持续拿地的能力，如果他手头只有一个较好的项目，我们则不会考虑。”欧美一家房地产基金的中国首席代表向南方周末记者解释他们的筛选标准。 

　　中国概念在2006年热得发烫，而房地产业则是热上加热。 

　　根据6月份国家统计局发布的报告预计，今年1-5月，在房地产开发企业的资金来源中，利用外资的增长速度异常惊人，高达89.9%。外资已经成为中国房地产市场持续高温的关键因素。在中央紧缩“银根”、紧缩“地根”双重政策出台后，国内房地产商有相当一部分面临着资金链断裂的危机。一些房地产集团谋求香港、新加坡、美国上市的传闻不断。而实力雄厚的境外投资公司摩根士丹利、高盛、麦格理(澳)、花旗、ING(荷)等顶级跨国投资公司，恰恰把国内房地产企业的“利空”视为自身进入的“利好”，迅速加快向中国房地产市场进军的步伐。 

　　6月，商务部联合国家外汇管理局又下发《商务部、国家外汇管理局关于进一步加强、规范外商直接投资房地产业审批和监管的通知》。通知强调，外商投资内地房地产项目必须通过审批。更复杂的审批程序可能导致一些潜在收购暂时中止，且政策对完成审批程序设立了更长的时间。 

　　“目前很多项目的审批已经暂停了，而且审批通过的时间和半年前相比也延迟了很多，长的需要半年的时间。”一位开发商对南方周末记者表示，“但是在商场上最重要的就是时间，没有一个项目可以等你半年以上，这样对开发商的资金能力提出了更高的要求。” 

　　“幸好，目前国内市场的资金比较充裕，通过信托代持股权的形式，我们还是可以完成整个项目转让，获得银行贷款，事实上，现在资金的杠杆比例很高。”他说道。 

　　对于开发商来说，目前的进入门槛提高了，即使现在以高价拿到了地，并不表示成为永远的赢家。 

　　“现在的要求更高了，对于土地本身你要对它进行合理的开发才能保证以后的旺销。”易居研究院上海机构副总经理于旦旦提到，“绿城的新江湾城的价格以后要定在2万以上，但是有多少本地市民愿意去那里居住呢？导入外来人口需要开发商赋予这个地块更多的理念，赋予产品更大的价值，包括将周围的配套做好才能吸引消费者。” 

　　中信证券房地产行业首席分析师王德勇说：“我们假定合理的房价收入比为250，根据2006年底房价水平，则全国、北京、上海80平方米商品房的合理月租金应为1080 元、2650元、2300元左右，略高于目前市场的平均租金，房价偏高的可能性较大。” 

　　而经济学家谢国忠日前指出：“中国资产市场依靠升值期望以弥补资产的低运营回报。人们买资产是因为相信他们可以以更高的价格卖给别人。如果资产运营回报很好并且资产价格在估值水平不升的情况下可以上升的话，这种想法是没有问题的。而当资产升值仅依赖于估值膨胀，这就是金字塔游戏，即泡沫。” 
 
