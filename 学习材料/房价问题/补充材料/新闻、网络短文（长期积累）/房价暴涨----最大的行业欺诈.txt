房价暴涨----最大的行业欺诈  

[ 生得云淡活得水暖 ] 于2007-08-09 12:41:41 上帖 [ 发短信 ] [ 表状 ]  

　　近年来，我国住房价格在持续的调控中越调越高，以致于连开发商也坦承：“现在楼盘的利润率已经高到让我们不好意思的程度了。”社会对此极度失望。为什么会造成如此败局呢？难道楼市无法调控吗？答案应该是否定的。 

　　上世纪90年代初，我国就有成功快速调控楼市的典范，其高祭的利器是金融信贷。金融对房地产的重要性，可以用“致命”来表达。金融如 水，房产似舟。水可载舟，亦可覆舟。房地产兴亦金融，衰亦金融。缘由极其简单，房地产是资本密集产业，离不开金融的支持。近年来对房地产的调控，主要仰仗 土地、税收政策，唯独不用金融工具。就职能部门而言，责成八部委(建设部、国土资源部、财政部、审计署、监察部、国家税务总局、国家发改委和国家工商局) 进行调控，而唯独遗漏中央银行。这样的思路，败局是注定的。因为离开了与房地产关系最紧密的金融调控，如何控制住房价格？直接限制肯定是行不通的。如今房 价不断攀升，表面上看好像有支付能力的需求过大，供给不足。实则,放贷存在监管漏洞---质押物价值的依据不足。如某楼盘一天之内涨价三次,涨幅超过千元，而银行只依据开发商同盟制定的购房合同价格放贷, 开发商同盟利用放贷漏洞---假按揭制造假市值,哄抬房价。一言以蔽之：住房信贷政策失控，信贷发放失控。因此要平抑当下 失控上涨的房价，必须果断地调控住房贷款政策，科学合理地控制住房贷款的投放。 

　　失控的房贷推高房价 

　　人们肯定还未忘记，上世纪90年代中后期，我国住房市场曾经经历过一个冰冻期，尽管当时房价不高，但还是少有人问津，这是因为居民缺乏 实际的购买力。商品房销售的春天，是在发放住房贷款之后才到来的，可见住房销售市场的命运一开始就与金融联系在一起。依靠贷款推动住房消费，经济学称之为 信用式住房消费，是信贷的一大功绩。但眼前信用漏洞缺乏监管式消费，必然造成走向反面。比如开发商同盟利用房贷价格依据漏洞制造假市值，哄抬房价，造成商品房高价位过旺需求假像，使其间真正消费变成了一批终生负债的房奴；再有可能给投机者提供契机，市场显得极度动荡。当下的商品房市场就处于这样的 状态，说明房贷政策已经不合理。住房信贷消费漏洞---存在明显行业欺诈，主动权不在消费者，而在信用的提供者（银行与开发商合谋）。这种信用目前均由金融机构以房贷形式提供。这也暴露出了一个 无法回避的问题：目前的住房贷款政策上有严重失误，将伤害住房产业的可持续发展；同时已经严重伤害了民众，必须马上调整。 

　　且看我国近年来房贷市场，实际上奉行着“敞口式、支撑性”原则。“敞口式”表现在多方面，在贷款的商品房上，表现为现房可贷，期房亦可 贷；小户型可贷，大房型亦可贷；平价房可贷，高价房亦可贷；一手房可贷，二手房亦可贷；就价格动态上讲，价格稳定不变(即开盘定价后至销售完)的可贷，价 格不断上涨的亦可贷；个人第一套房可贷，第二套房亦可贷；本地户籍者可贷，外来者亦贷。再就一个地区或一家金融机构而言，对房贷缺乏时点进度控制，以及贷 款量的控制(既无相对量又无绝对量控制)。 

　　这种状况反映出房贷实际上处于“敞口式”的供应。比如2003年第4季度，上海新增贷款中房贷占了六、七成，而且集中井喷，贷款如此投 放状态，无论从哪方面衡量都是不正常的。由于“敞口式”的无节制房贷，房贷就成了住房销售市场持续兴旺，价格不断上涨的支撑性物质基础。这样的支撑性，演 绎成了房价与房贷相互联动的机制：房产商为追逐暴利，千方百计拉高房价；银行为利息，紧随其后，房贷规模随房价上涨而最佳扩张。房价拉着贷款走，贷款推着 房价涨。二者之所以如此默契，因为有利可图，而且趁着房贷无控制的好时机！ 

　　“敞口式”房贷抵消调控效力 

　　信贷支持经济(无疑包括房地产经济)上，各个不同时期都有倾向性，这种倾向性由宏观经济中的矛盾决定的，于是就有了信贷支持什么？限制 什么？就有了信贷政策。无倾向性地发放贷款是不符合经济发展需要不断调整的规律的。如时下的生产性贷款，就是有保有压的。对节能减排不符合规定的就停贷。 如果贷款只讲一般的商业原则，即如期收回本息，则有可能损害了宏观利益。近年来的住房信贷放弃了对住房消费的合理引导和必要控制，从而产生相当严重的负面 影响，突出问题就是实际上支撑了房价的非理性上涨。 

　　1、社会改善居住消费的需求，超越了经济发展的速度，给市场形成了超前的压力。居住消费是高投入的消费，需要消耗大量的资源，包括土 地。就全社会而言，整体的居住水准，是与生产力发展水平相适应的。所以居住消费的改善，应该与经济发展同步，是一个漫长而渐进的过程，不可能实现于朝夕之 间。近年来，由于信用消费方式上提倡，加之贷款的易得性，客观上极大地刺激了民众改善居住条件的欲望。于是住房供求平衡一再被打破，虽供给大增也赶不上需 求的增速。假如从房贷着手对此进行约束，比如仅供给保障性住房需求的购房者，其余改善性需求的自行解决，则部分需求就会推迟，供求矛盾就会缓解。用现钱去 购房，购房者就不会去买高价的账。 

　　2、房贷偏离超越了保障性住房需求方向，刺激了居住消费的高标准化。住房建设的特点需要占有土地资源。土地资源属不可再生资源，弥足珍 贵。因此，国民居住标准(特别是面积)既要与本国生产力水平相匹配，又需土地资源的承载力所许可。如日本、香港地区很富裕，但土地资源稀缺，因此住房面积 都不大。我国内地总体上人均土地低于国际水准，然而近年来却造了不少大房子。大房子的供给，是一个脱离了国情的错误倾向，刺激了居民的消费欲望。这类住房 属改善型需求，有的甚至属奢侈型。如将大房子拒之于贷款之外，那么房价亦就难以推到如此之高。 

　　3、年轻人的一步到位的消费方式，超前增加了住房总需求。现在刚上班的青年人，购房动辄140-150平方米，所谓一步到位。这种消费 方式无疑是浪费式的，至少前十年是利用率不高，因为没有孩子或孩子很小。但由于房贷的易得性，刺激了青年人的这种消费方式，客观上又增加了一种需求压力。 这种压力，完全是由时下的房贷引发的。上述一系列信用式消费的消极因素的累积，快速激发了供求矛盾，形成了房价持续上涨的拉力。 

　　新政策应体现渐进、保障、民生 

　　调控楼市新政策的核心可概括为三点原则：第一，体现民生性，解决社会最广大的基本民众的住房需求，属雪中送炭，而非锦上添花；第二，支 持小康型安居消费，倡导节约型符合国情的居住消费；第三，逐步实现的原则。保障型小康式居住消费的实现，不应该也不可能在一个短期内到位，必须逐年解决一 部分，与住房建设速度相配合，因此房贷每年应有总量控制。奉行上述三大原则的房贷政策，既发挥了支持住房产业的持续发展，又平稳地渐进式解决了民众的住房 需求，同时又履行了调控住房市场的功能，房价亦可理性化。 

　　上述仅是保障型房贷政策的框架原则，实施的话须进行调查研究，严密论证，直到实施细则，并在实践中不断完善。笔者在此提供若干建议，抛 砖引玉。1、确定住房贷款总新增贷款中的比例。根据历年调控教训，此项措施必须作为指令性指标，由各地银监部门考核监督。此比例的依据，应是住房业总国民 经济中该有的相应比例；2、每年全国住房贷款总量控制，总量依据是每年小康型住房可供量。原则是供求平衡，供略大于求；3、房贷发行设置进度指标，避免井 喷式放贷，以免破坏供求关系；4、每户发放一次购房贷款，禁贷第二次贷款；5、高价房一律不贷，所谓高价房，指可获暴利的房价即高价；6、大面积房不贷， 具体超过100平方米房不贷；7、别墅类高档房不贷；8、价格变动房不贷，实施支持一房一价，即开盘定价后至售完不再涨价；9、贷现房不贷期房；10、严 格城市外来者房贷。 

　　如果上述房贷政策得以实施，可以断言，我国当前住房价格失控的局面定能改观。由于房地产利益涉及方方面面，因此新政的确立和推行，需要 决策部门力排众议，扫除障碍。此新政从短期利益衡量，开发商与银行失去最多(目前的暴利实属不义)，但从长远看，有利于房地产业的可持续发展，也有利于金 融的安全，因此开发商与银行业都该“风物长宜放眼量”，以求民生利益与企业利益共获，奉献和谐社会。

